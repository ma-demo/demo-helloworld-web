#!/bin/bash

# on VM "dev01"
# docker run -d -p "8500:8500" --hostname "consul" progrium/consul -server -bootstrap

KEY_STORE_IP="192.168.99.100"
KEY_STORE_PORT="8500"

docker-machine create  \
--driver virtualbox    \
--swarm                \
--swarm-master         \
--swarm-discovery="consul://$KEY_STORE_IP:$KEY_STORE_PORT"          \
--engine-opt="cluster-store=consul://$KEY_STORE_IP:$KEY_STORE_PORT" \
--engine-opt="cluster-advertise=eth1:2376"                          \
ws-master

docker-machine create  \
--driver virtualbox    \
--swarm                \
--swarm-discovery="consul://$KEY_STORE_IP:$KEY_STORE_PORT"          \
--engine-opt="cluster-store=consul://$KEY_STORE_IP:$KEY_STORE_PORT" \
--engine-opt="cluster-advertise=eth1:2376"                          \
ws-slave01

docker-machine create  \
--driver virtualbox    \
--swarm                \
--swarm-discovery="consul://$KEY_STORE_IP:$KEY_STORE_PORT"          \
--engine-opt="cluster-store=consul://$KEY_STORE_IP:$KEY_STORE_PORT" \
--engine-opt="cluster-advertise=eth1:2376"                          \
ws-slave02

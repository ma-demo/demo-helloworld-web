#!/bin/bash

echo "############################################################"
echo "### pre.sh => begin"
echo "###"

if [ -f "/etc/ansible/ansible.cfg" ]; then
	echo "Install Ansible ... => done already"
else
  echo "Install pre-requisits ..."
  apt-get update
  apt-get install software-properties-common -y

  echo "Refreshing package list ..."
  apt-add-repository ppa:ansible/ansible-1.9
  apt-get update

  echo "Installing ansible ..."
  apt-get install ansible -y
fi

echo "###"
echo "### pre.sh => finished"
echo "############################################################"

#!/bin/bash

mkdir /tmp/vbox
mount -o loop /home/vagrant/VBoxGuestAdditions_5.0.16.iso /tmp/vbox
yes | /tmp/vbox/VBoxLinuxAdditions.run
umount /tmp/vbox
rmdir /tmp/vbox
ln -s /opt/VBoxGuestAdditions-*/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions

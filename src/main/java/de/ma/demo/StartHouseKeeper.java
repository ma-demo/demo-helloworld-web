package de.ma.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StartHouseKeeper implements ApplicationListener<ApplicationEvent>
{
	private transient Logger log = LoggerFactory.getLogger( this.getClass() );
	
	@Override
	public void onApplicationEvent( ApplicationEvent event )
	{
		this.log.debug( "event: {}", event );
	}
}

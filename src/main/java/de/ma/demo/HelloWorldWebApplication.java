package de.ma.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class HelloWorldWebApplication extends SpringApplication 
{
	protected transient Logger log = LoggerFactory.getLogger( this.getClass() );
	
	public HelloWorldWebApplication()
	{
		// used in unit tests
	}
	
	public HelloWorldWebApplication( Object... sources )
	{
		super( sources );
	}
	
	@Override
	protected void logStartupProfileInfo( ConfigurableApplicationContext context )
	{
		super.logStartupProfileInfo( context );
		
		Environment env = context.getEnvironment();
		String      datasourceUrl      = env.getProperty( "spring.datasource.url", "" );
		String      datasourceUsername = env.getProperty( "spring.datasource.username", "" );
		
		this.log.debug( "spring.datasource.url     : {}", datasourceUrl );
		this.log.debug( "spring.datasource.username: {}", datasourceUsername );
	}
	
	public static ConfigurableApplicationContext run( Object source, String[] args )
	{
		return new HelloWorldWebApplication( source ).run( args );
	}
	
	public static void main(String[] args) throws InterruptedException
	{
		Thread.sleep( 5000L );
		HelloWorldWebApplication.run( HelloWorldWebApplication.class, args );
	}
}
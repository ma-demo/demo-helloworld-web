package de.ma.demo;

import java.util.ArrayList;
import java.util.List;

public class ConfigProperties {
	
	/** property 01 */ String prop01;
	/** property 02 */ String prop02;
	/** property 03 */ String prop03;
	
	/** demo list   */ List<String> entries = new ArrayList<>();
	
	public List<String> getEntries(){
		return this.entries;
	}
	
	public String getProp01() {
		return prop01;
	}
	public void setProp01(String prop01) {
		this.prop01 = prop01;
	}
	public String getProp02() {
		return prop02;
	}
	public void setProp02(String prop02) {
		this.prop02 = prop02;
	}
	public String getProp03() {
		return prop03;
	}
	public void setProp03(String prop03) {
		this.prop03 = prop03;
	}
}

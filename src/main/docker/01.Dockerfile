FROM nginx:latest
MAINTAINER Bernd Fischer "bfischer@mindapproach.de"
ENV   MODIFIED_AT     2016-04-14_1230
ENV   VERSION         1.0
LABEL de.aemc.version=1.0

RUN echo "Hallo aus dem Dockerfile" > /usr/share/nginx/html/index.html

ENTRYPOINT [ "/usr/sbin/nginx" ]
# ENTRYPOINT [ "/usr/sbin/nginx", "-g", "daemon off;" ]
CMD [ "-h" ]

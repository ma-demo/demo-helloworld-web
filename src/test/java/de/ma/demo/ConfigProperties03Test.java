package de.ma.demo;

import static org.junit.Assert.*;

import java.util.List;

import static org.hamcrest.Matchers.*;  

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration
@ActiveProfiles( "propertytests" )
public class ConfigProperties03Test {
	
	@Inject ConfigProperties props;
		
	@Test public void test01() {
		assertThat( this.props, is( notNullValue() ) );
		
		List<String> entries = this.props.getEntries();
		
		assertThat( entries, is( notNullValue() ) ) ;
		assertThat( entries.size(), is( 3 ) ) ;
		assertThat( entries.get( 0 ), is( "entry01" ) );
		assertThat( entries.get( 1 ), is( "entry02" ) );
		assertThat( entries.get( 2 ), is( "entry03" ) );
	}

	// --------------------------------------------------------------------------------------------------------
	
	@Configuration
	@EnableConfigurationProperties
	static class Config {
		
		@ConfigurationProperties( prefix="demo" )
		@Bean public ConfigProperties props() {
			return new ConfigProperties();
		}
	}
}

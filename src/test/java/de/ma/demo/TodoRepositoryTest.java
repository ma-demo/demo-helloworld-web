package de.ma.demo;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = HelloWorldWebApplication.class )
@ActiveProfiles( "dev" )
public class TodoRepositoryTest
{
	@Inject
	TodoRepository todoRepo;
	
	@Test
	public void test()
	{
		assertThat( this.todoRepo        , is( notNullValue() ) );
		
		long countBefore = this.todoRepo.count();
		
		Todo todo = this.todoRepo.save( new Todo( "Task1", "mach was" ) );
		
		assertThat( todo        , is( notNullValue() ) );
		assertThat( todo.getId(), is( notNullValue() ) );
		
		assertThat( this.todoRepo.count(), is( countBefore + 1 ) );
		
		assertThat( this.todoRepo.findOne( todo.getId() ).getId()     , is( todo.getId() ) );
		assertThat( this.todoRepo.findOne( todo.getId() ).getTitle()  , is( todo.getTitle() ) );
		assertThat( this.todoRepo.findOne( todo.getId() ).getContent(), is( todo.getContent() ) );
	}

}

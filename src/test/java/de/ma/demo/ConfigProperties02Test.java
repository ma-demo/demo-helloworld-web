package de.ma.demo;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Map;

import static org.hamcrest.Matchers.*;  

import javax.inject.Inject;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration
@ActiveProfiles( "propertytests" )
public class ConfigProperties02Test {
	
	private static final String DEMO_PROP01_VAL = "Hallo01 from Yaml-File";
	
	private static final String DEMO_PROP02_KEY = "DEMO_PROP02";
	private static final String DEMO_PROP02_VAL = "Hallo02 from SystemProperty";
	private static final String DEMO_PROP03_KEY = "DEMO_PROP03";
	private static final String DEMO_PROP03_VAL = "Hallo03 from Environment";
	
	@Inject ConfigProperties props;
	
	@BeforeClass static public void beforeClass() throws Exception {
		
		// simulating an external configured environment variable
		assertThat( System.getenv( DEMO_PROP03_KEY ), is( nullValue() ) );
		setEnvironmentVariable   ( DEMO_PROP03_KEY, DEMO_PROP03_VAL );
		assertThat( System.getenv( DEMO_PROP03_KEY ), is( DEMO_PROP03_VAL ) );
		
		System.setProperty( DEMO_PROP02_KEY, DEMO_PROP02_VAL );
	}
	
	// setting values via System.setProperty() works only inside beforeClass()
	@Before public void before() {
		System.setProperty( DEMO_PROP02_KEY, "Hallo02 from SystemProperty before" );
	}
	
	@Test public void test01() {
		assertThat( this.props, is( notNullValue() ) );
		assertThat( this.props.getProp01(), is( DEMO_PROP01_VAL ) );
		assertThat( this.props.getProp02(), is( DEMO_PROP02_VAL ) );
		assertThat( this.props.getProp03(), is( DEMO_PROP03_VAL ) );
	}

	// --------------------------------------------------------------------------------------------------------
	
	// inspired from 
	// http://stackoverflow.com/questions/318239/how-do-i-set-environment-variables-from-java
	
	private static void setEnvironmentVariable( String name, String value ) throws Exception {
		
		Assert.hasText( name , "argument name must not be null or empty" );
		Assert.hasText( value, "argument value must not be null or empty" );
		
		Map<String, String> env = System.getenv();
		
		Class<?> clazz = env.getClass();
		Field    field = clazz.getDeclaredField( "m" );
		field.setAccessible( true );
		
		@SuppressWarnings("unchecked")
		Map<String, String> obj = ( Map<String, String> ) field.get( env );
		obj.put( name, value );
	}
	
	// --------------------------------------------------------------------------------------------------------
	
	@Configuration
	@EnableConfigurationProperties
	static class Config {
		
		@ConfigurationProperties( prefix="demo" )
		@Bean public ConfigProperties props() {
			return new ConfigProperties();
		}
	}
}

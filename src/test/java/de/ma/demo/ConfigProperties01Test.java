package de.ma.demo;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;  

import javax.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration()
// @TestPropertySource seems not to work property-files only
// yaml-files aren't taken into account
@TestPropertySource( 
	locations="classpath:app-eval.properties",
	properties= { "demo.prop02: Hallo from TestPropertySource" } 
)
public class ConfigProperties01Test {
	
	@Inject ConfigProperties props;
	
	// neither environment nor system variables are "recognized"
	// in this concrete scenario
	@BeforeClass static public void beforeClass() {
		System.setProperty( "DEMO_PROP02", "Hallo from SystemProperty" );
	}
	
	@Test public void test01() {
		assertThat( this.props, is( notNullValue() ) );
		assertThat( this.props.getProp01(), is( "Hallo01" ) );
		assertThat( this.props.getProp02(), is( "Hallo from TestPropertySource" ) );
	}
	
	@Configuration
	@EnableConfigurationProperties
	static class Config {
		
		@ConfigurationProperties( prefix="demo" )
		@Bean public ConfigProperties props() {
			return new ConfigProperties();
		}
	}
}


node {
  stage name: 'Step 1 - Build and Unit Tests', concurrency: 1
  
  git url: 'https://gitlab.com/ma-demo/demo-helloworld-web.git'
  def mvnHome = tool 'M3.3.9'
  
  sh "${mvnHome}/bin/mvn clean package -DskipTests=true"
  
  stage name: 'QS Gate 2 - Build DockerImage', concurrency: 1
  
  def strVersion       = getVersionStringFromPom()
  def majorVersion     = getMajorVersion( strVersion )
  def minorVersion     = getMinorVersion( strVersion )
  def strGitProperties = readFile( 'target/generated-resources/git.properties' )
  def commitTimestamp  = getCommitTimestamp( strGitProperties )
  def newVersion       = "$majorVersion.$minorVersion-$commitTimestamp"
  
  echo "strVersion     : $strVersion"
  echo "majorVersion   : $majorVersion"
  echo "minorVersion   : $minorVersion"
  echo "commitTimestamp: $commitTimestamp"
  echo "newVersion     : $newVersion"
  
  echo "DOCKER_HOST      : ${env.DOCKER_HOST}"
  echo "DOCKER_TLS_VERIFY: ${env.DOCKER_TLS_VERIFY}"
  echo "DOCKER_CERT_PATH : ${env.DOCKER_CERT_PATH}"
  echo "DOCKER_REGISTRY  : ${env.DOCKER_REGISTRY}"
  
  sh "${mvnHome}/bin/mvn resources:copy-resources docker:build -DnewVersion=$newVersion -PbuildDockerImage"
  sh "${mvnHome}/bin/mvn docker:tag -PtagDockerImage -DnewVersion=$newVersion -DdockerRegistry=${env.DOCKER_REGISTRY}"
  sh "${mvnHome}/bin/mvn docker:push -PpushDockerImage -DnewVersion=$newVersion -DdockerRegistry=${env.DOCKER_REGISTRY}"
}

// ==========================================================================================================

def getVersionStringFromPom()
{
	def matcher = readFile( 'pom.xml' ) =~ '<version>(.+)</version>'
	matcher ? matcher[0][1] : null
}

def String getMinorVersion( String str )
{
	def matcher = str =~ '(\\d+)[.-](\\d+)[.-](.*)'
	matcher ? matcher[0][2] : null
}

def String getMajorVersion( String str )
{
	def matcher = str =~ '(\\d+)[.-](\\d+)[.-](.*)'
	matcher ? matcher[0][1] : null
}

def String getCommitTimestamp( String str )
{
	def matcher = str =~ 'git.commit.time=(.+)'
	matcher ? matcher[0][1] : null
}
